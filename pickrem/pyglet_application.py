import pyglet
from pyglet.window import Window

from pickrem import application
from pickrem.simulation import client_simulation


class PygletApplication(application.Application):
    def __init__(self):
        super().__init__(client_simulation.run_test)

    def start(self):
        # start application stuff
        super().start()
        pyglet.app.run()
        self.clock = pyglet.clock

    def quit(self):
        self.cleanup()

    def _create_controller(self):
        from pickrem.application_controller import ApplicationController

        return ApplicationController(self)

    def _create_renderer(self, display_surface, pipe_to_simulation):
        from pickrem.renderer.renderer import Renderer

        return Renderer(display_surface, pipe_to_simulation)

    def _create_display_surface(self):
        self._display_surface = Window(*self.window_size)
        self._display_surface.on_draw = self.on_draw
        return self._display_surface

    def resize_window(self, width, height):
        # self._display_surface.on_resize(width, height)
        self.renderer.camera.surface = self._display_surface

    def cleanup(self):
        super().cleanup()
        pyglet.app.exit()

    def execute(self):
        self.start()
        pyglet.clock.schedule_interval(self._update, 1 / 120.0)
        pyglet.app.run()

    def _update(self, delta_time):
        self.controller.update()

    def on_draw(self, delta_time):
        self._display_surface.clear()
        self.renderer.frame(delta_time)


if __name__ == '__main__':
    app = PygletApplication()
    app.execute()