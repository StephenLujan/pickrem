from pickrem.resources import loader



# TODO: a time_into_action field may be needed in init and update, but then again YAGNI
class Renderable(object):
    """
    This contains only the data and methods needed to draw something in the renderer. Generally, game logic is performed
    elsewhere, while this simply represents the visual result.
    """

    def __init__(self, graphics_resource_id, position_x, position_y, scale_x, scale_y, rotation, action):
        self.visual = loader.load(graphics_resource_id)
        self.graphics_resource_id = graphics_resource_id
        self.position_x = position_x
        self.position_y = position_y
        assert (scale_x > 0.0)
        assert (scale_y > 0.0)
        self.scale_x = scale_x
        self.scale_y = scale_y
        self.rotation = rotation
        self.visual.action = action
        # self._time_into_action = time_into_action

    def full_update(self, delta_time, graphics_resource_id, position_x, position_y, scale_x, scale_y, rotation, action):
        # nothing special to do for these
        self.position_x = position_x
        self.position_y = position_y
        self.scale_x = scale_x
        self.scale_y = scale_y
        self.rotation = rotation
        if self.graphics_resource_id != graphics_resource_id:
            self.visual = loader.load(graphics_resource_id)
            self.graphics_resource_id = graphics_resource_id

        self.visual.action = action
        # self._time_into_action = time_into_action
        self.visual.update(delta_time)

    def update(self, delta_time):
        self.visual.update(delta_time)

    def draw(self, camera):
        self.visual.blit(camera, self.position_x, self.position_y, self.scale_x, self.scale_y, self.rotation)


import os
import unittest


class RenderableTest(unittest.TestCase):
    @unittest.skipIf(os.environ.get('RUNALL'), "manually validated test")
    def test_visually_confirm_sprite(self):
        import time
        import pygame
        from pickrem.resources import loader
        from pickrem.animation.pygame.camera import Camera
        from pickrem.animation.sprite import Sprite
        from pickrem.animation.pygame.caching_image import CachingImage

        pygame.init()
        display_surface = pygame.display.set_mode((600, 450), pygame.HWSURFACE | pygame.DOUBLEBUF)
        cam = Camera(display_surface, 0, 0)

        visual_id = loader.sprites['link']
        renderable = Renderable(visual_id, 50, 0, 1.0, 1.0, 0, '')
        self.assertIsInstance(renderable.visual, Sprite)
        self.assertIsInstance(renderable.visual.image, CachingImage)
        self.assertIsInstance(renderable.visual.image._final_image, pygame.SurfaceType)

        renderable.draw(cam)

        pygame.display.flip()
        for x in range(50):
            pygame.event.pump()
            time.sleep(.10)