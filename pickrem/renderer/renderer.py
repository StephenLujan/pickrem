from multiprocessing.connection import PipeConnection
from multiprocessing.connection import Pipe
import logging
import pickle
import unittest
import math
import os

from pickrem.renderer.renderable import Renderable
from pickrem.animation.camera import Camera


logging.basicConfig(level=logging.DEBUG)
logger = logging.Logger("renderer")


def pack(object):
    return pickle.dumps(object)


def unpack(message):
    return pickle.loads(message)


class Renderer():
    """
    The Renderer will draw each frame to the desired pygame display surface. This only handles logic related to display
    such as creating new Visuals, a virtual camera, advancing the frames in animations, and switching between
    animations for Actors that use many. The actual game state is calculated elsewhere, converted into messages that
    correspond to the methods in Renderable, and piped here so that Renderer knows what to draw and where.
    """

    def __init__(self, display_surface, simulation_pipe: PipeConnection):
        assert display_surface is not None
        self.display_surface = display_surface
        self.simulation_pipe = simulation_pipe
        self.renderables = {}
        self.time_multiplier = 1.0
        self.paused = False
        self.camera = Camera(400, 300)

    def camera_track(self, renderable_key):
        self.camera.track(self.renderables[renderable_key])

    def cleanup(self):
        self.renderables = {}

    def _renderables_update(self, delta_time, update):
        renderables = self.renderables
        for key, value in update.items():
            if key in renderables:
                renderables[key].full_update(delta_time, *value)
                logger.debug('updating renderable %i', key)

        # add and remove renderables to match content of message
        additions = {key: value for key, value in update.items() if key not in self.renderables}
        for key, value in additions.items():
            renderables[key] = Renderable(*value)
            logger.debug('adding renderable %i', key)
        removals = [key for key in self.renderables if key not in update]
        for key in removals:
            logger.debug('removing renderable %i', key)
            del renderables[key]


    def _update(self, delta_time):
        """
        process all messages from the pipe and update our state as needed
        :param delta_time:
        :return: the number of messages processed
        """

        # Updates the camera's position every frame
        self.camera.update(delta_time)

        # Use a modified delta_time for rendering behavior reflective of the simulation state, not for things like
        # moving around the camera.
        if self.paused:
            delta_time = 0
        else:
            delta_time *= self.time_multiplier

        # capture the latest state only from the simulation and don't even unpack the rest
        message = None
        message_count = 0
        callbacks = {}
        while self.simulation_pipe.poll(0):
            try:
                message = self.simulation_pipe.recv()  # Read from the output pipe and do nothing
                message_count += 1


                # TODO  This is not a very forward-thinking method here.
                if message is not None:
                    data = unpack(message)
                    # print (data)
                    for key, value in data[0].items():
                        callbacks[key] = value

            except EOFError:
                break

        # make sure we actually have a message then unpack and process it
        if message is not None:
            data = unpack(message)
            # Feed update info into our existing renderables.
            self._renderables_update(delta_time, data[0])
            for key in callbacks:
                if key == "tracking_target":
                    self.camera_track(callbacks[key])

        elif delta_time and not self.paused:
            for renderable in self.renderables.values():
                renderable.update(delta_time)

        return message_count

    def frame(self, delta_time: float):
        """
        handles the display logic of the simulation and drawing for a single frame
        :param delta_time: The amount of time, in milliseconds, since the last frame. This will be used to do things
        like progressing animations over time. This does not control the buffer because renderer only draws the
        simulation and may not draw to the entire screen. The buffer must still be flipped elsewhere.
        :return: Nothing
        """
        self._update(delta_time)
        self.render()

    def render(self):
        raise NotImplemented

    def _draw(self):
        cam = self.camera
        # sort for proper render order
        if cam.rotation == 0:
            renderables = sorted(self.renderables.values(), key=lambda renderable: -renderable.position_y)
        else:
            radians = math.radians(-cam.rotation)
            sin, cos = math.sin(radians), math.cos(radians)
            renderables = sorted(self.renderables.values(),
                key=lambda renderable: renderable.position_x * sin + renderable.position_y * cos)
        # draw all renderables
        for renderable in renderables:
            renderable.draw(cam)


class PygameRenderer(Renderer):
    def __init__(self, display_surface, simulation_pipe: PipeConnection):
        from pickrem.animation.pygame.camera import Camera

        super().__init__(display_surface, simulation_pipe)
        self.camera = Camera(display_surface)

    def render(self):
        """
        Redraws everything to the Renderer's display surface. This does not control the buffer because renderer only
        draws the simulation and may not draw to the entire screen. The buffer must still be flipped elsewhere.
        :return: Nothing
        """
        self.display_surface.fill((0, 0, 0))
        self._draw()


class PygletRenderer(Renderer):
    def __init__(self, display_surface, simulation_pipe: PipeConnection):
        from pickrem.animation.pyglet.camera import Camera

        super().__init__(display_surface, simulation_pipe)
        self.camera = Camera(display_surface)

    def render(self):
        """
        Redraws everything to the Renderer's display surface.
        :return: Nothing
        """
        # self.display_surface.fill((0, 0, 0))
        self._draw()


class TestRenderer(unittest.TestCase):
    def setUp(self):
        self.pipe_to_renderer, self.pipe_from_renderer = Pipe()
        self.renderer = Renderer("test", self.pipe_from_renderer)

    def test_process_messages(self):
        test_message = pack([{1: (0, 10.0, 20.0, 1.0, 1.0, 0.0, None)}, "jibberjabber"])

        self.renderer._update(0.1)
        self.pipe_to_renderer.send(test_message)
        self.pipe_to_renderer.send(test_message)
        self.pipe_to_renderer.send(test_message)
        self.renderer._update(0.1)
        self.renderer._update(0.1)

    def test_add_remove_update(self):
        self.renderer.renderables = {}
        message1 = pack([{1: (0, 10.0, 20.0, 1.0, 1.0, 0.0, None), 2: (1, 10.0, 20.0, 1.0, 1.0, 0.0, None)}])
        self.pipe_to_renderer.send(message1)
        self.renderer._update(0.1)
        self.assertEqual(len(self.renderer.renderables), 2)

        message2 = pack([{2: (1, 20.0, 20.0, 1.0, 1.0, 0.0, None)}])
        self.pipe_to_renderer.send(message2)
        self.renderer._update(0.1)
        self.assertEqual(len(self.renderer.renderables), 1)
        self.assertEqual(self.renderer.renderables[2].position_x, 20.0)


class TestPyGameRenderer(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        import pygame

        print('setting up')
        pygame.init()
        cls.display_surface = pygame.display.set_mode((500, 400), pygame.HWSURFACE | pygame.DOUBLEBUF)

    @classmethod
    def tearDownClass(cls):
        import pygame

        cls.display_surface = None
        pygame.quit()

    def setUp(self):
        self.pipe_to_renderer, self.pipe_from_renderer = Pipe()
        self.renderer = PygameRenderer(self.display_surface, self.pipe_from_renderer)

    def tearDown(self):
        self.renderer.cleanup()

    def test_can_load_renderer(self):
        pass

    def test_empty_frame(self):
        self.renderer.frame(0.1)

    @unittest.skipIf(os.environ.get('RUNALL'), "manually validated test")
    def test_visual_confirmation(self):
        from pickrem.animation.four_way_animation import FourWayAnimation
        from pickrem.animation.sprite import Sprite
        from pickrem.resources import loader
        import time

        visual_id = loader.four_way_animations['link']['walk']
        four_way_animation = Renderable(visual_id, 50, 0, 1.0, 1.0, 0.0, '')
        self.assertIsInstance(four_way_animation.visual, FourWayAnimation)
        self.renderer.renderables[1] = four_way_animation
        visual_id = loader.sprites['link']
        sprite = Renderable(visual_id, -50, 0, 1.0, 1.0, 0.0, '')
        self.assertIsInstance(sprite.visual, Sprite)
        self.renderer.renderables[2] = sprite
        self.assertEqual(sprite.position_x, -50)
        import pygame

        for x in range(50):
            self.renderer.frame(0.1)
            pygame.display.flip()
            time.sleep(0.1)
            pygame.event.pump()