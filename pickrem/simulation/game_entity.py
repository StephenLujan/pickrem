import abc


class GameEntity(object):
    """
    The common parent of everything in a simulation. This contains methods and data that empower memory management,
    client/server synchronicity, and
    """
    __metaclass__ = abc.ABCMeta

    class State():
        # references to this GameEntity should be removed so it can be released from memory
        removable = 0
        # requires resources to be loaded
        loading = 1
        # in active normal use
        alive = 2
        # this entity is no longer active but must be kept around
        dead = 3

    number_created = 0

    all = {}  # holds all GameEntities in existence
    alive = {}  # optimization for many processes looking for alive entities

    @staticmethod
    def prune():
        """this removes all GameEntities with state == removable from GameEntity.all"""
        GameEntity.all = {key: value for key, value in GameEntity.all.items() if value.state}
        GameEntity.alive = {key: value for key, value in GameEntity.all.items() if
                            value.state == GameEntity.State.alive}

    def __init__(self, name="Unnamed GameEntity"):
        GameEntity.number_created += 1
        self.id = GameEntity.number_created
        self.state = self.State.alive
        self.name = name
        self.group = None
        GameEntity.all[self.id] = self

    @abc.abstractmethod
    def update(self, delta_time):
        """
        This method will update as needed all states of the GameEntity unrelated to visual display. Ideally this method
        may be completely decoupled from render, and able to run at a different rate.

        :param delta_time: the time elapsed in the last update cycle
        :return: nothing
        """