import logging

from pygame.constants import *

from pickrem.controls.pygame_controller import PygameController as GameEngineController
from pickrem.simulation.actor import Actor
from pickrem.physics.self_propelled import SelfPropelled

# logger = multiprocessing.get_logger()
logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger("controls")
# logger.setLevel(logging.DEBUG)


class PlayerController(GameEngineController):
    """
    The Controller reacts to external input from the system or user.
    """

    def __init__(self, player: Actor):
        assert (isinstance(player, Actor))
        assert (isinstance(player.body, SelfPropelled))
        self.player = player
        super().__init__()

    def update(self):
        super().update()
        x, y = 0, 0
        pressed = self.is_key_pressed
        if pressed(K_UP):
            y += 1
        if pressed(K_DOWN):
            y -= 1
        if pressed(K_RIGHT):
            x += 1
        if pressed(K_LEFT):
            x -= 1
        if len(self.joysticks) and x == 0 and y == 0:
            x = self.joysticks[0].check_axis(0)
            # most likely the joystick is controller and not a joystick, so invert y
            y = -self.joysticks[0].check_axis(1)

        # print (x,y)
        self.player.body.control(x, y)
