import math
import logging

from pickrem.simulation.game_entity import GameEntity
from pickrem.physics.body import Body


logging.basicConfig(level=logging.DEBUG)
logger = logging.Logger("actor")


class Actor(GameEntity):
    def __init__(self, body: Body, visual_resource_id: int, name="Unnamed Character", scale_x=1.0, scale_y=1.0,
                 rotation=0.0):
        super().__init__(name)
        self.visual_resource_id = visual_resource_id
        self.body = body
        self.visual_action = None
        # TODO: use the body and physics engine for rotation at least
        self.scale_x = scale_x
        self.scale_y = scale_y
        self._rotation = rotation

    # def update(self, delta_time):
    # """
    #     This method will update as needed all states of the Character unrelated to visual display.
    #
    #     :param delta_time: the time elapsed in the last update cycle
    #     :return: nothing
    #     """
    #     self.body.update(delta_time)
    #     logger.debug('update')
    #     if not self.body.velocity.is_zero:
    #         self.rotation = self.body.velocity.angle * 180.0 / math.pi
    #         logger.debug('rotation', self.rotation)

    @property
    def rotation(self):
        if not self.body.velocity.is_zero:
            self._rotation = self.body.velocity.angle * 180.0 / math.pi
        return self._rotation

    def get_renderable(self):
        """
        returns a series of values that can be used to build a Renderable in the renderer process
        :return: visual_resource_id, position_x, position_y, scale_x, scale_y, rotation, and action
        """
        return (
            self.visual_resource_id,
            self.body.position.x,
            self.body.position.y,
            self.scale_x,
            self.scale_y,
            self.rotation,
            self.visual_action
        )