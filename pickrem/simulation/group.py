import unittest
import weakref
import gc
import logging

from pickrem.simulation.game_entity import GameEntity
from pickrem.physics.body import Body
from pickrem.simulation.actor import Actor


logging.basicConfig(level=logging.DEBUG)
logger = logging.Logger("Group")


class Group(GameEntity):
    def __init__(self, name="Unnamed Group"):
        super().__init__(name)
        self._entities = weakref.WeakValueDictionary()

    def add(self, addition: GameEntity):
        """Adds a GameEntity to this Group"""
        # sanity check still won't prevent a circular loop of more than 2
        assert (not (isinstance(addition, Group) and self in addition))

        # don't let entities exist in more than one group
        if addition.group is not None:
            addition.group.remove(addition)
        self._entities[addition.id] = addition
        addition.group = self

    def remove(self, removal: GameEntity):
        """
        Removes the input GameEntity from this Group. This method should not used often as GameEntities with the state
        removable will be automatically removed
        """
        del self._entities[removal.id]

    def update(self, delta_time):
        """
        this will update all GameEntities in the group and remove any GameEntities whose state == removable
        :param delta_time: the delta_time to be passed to updates
        :return: nothing
        """
        logger.debug('update()')
        for entity in self._entities.values():
            entity.update(delta_time)

    def __contains__(self, entity):
        return entity in self._entities

    def __getitem__(self, entity_id):
        return self._entities[entity_id]


class GroupTests(unittest.TestCase):
    def setUp(self):
        GameEntity.all = {}
        GameEntity.alive = {}

    def test_update_remove_prune(self):
        import time

        group = Group()
        actor1 = Actor(Body(), 0)
        group.add(actor1)
        actor2 = Actor(Body(), 0)
        group.add(actor2)
        actor3 = Actor(Body(), 0)
        group.add(actor3)

        self.assertEqual(len(GameEntity.all), 4)  # includes the Group itself
        self.assertEqual(len(group._entities), 3)
        group.update(1.0)
        self.assertEqual(len(group._entities), 3)
        actor2.state = GameEntity.State.removable
        GameEntity.prune()
        self.assertEqual(len(GameEntity.all), 3)
        gc.collect()
        time.sleep(0.1)
        group.update(1.0)
        self.assertTrue(actor2.id not in group._entities)
        self.assertEqual(len(group._entities), 2)

