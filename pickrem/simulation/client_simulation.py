from multiprocessing.connection import PipeConnection, Pipe
import multiprocessing
import time
import unittest
import gc
import pickle
import queue
import itertools

from pickrem.simulation.group import Group
from pickrem.simulation.game_entity import GameEntity
from pickrem.resources import loader
from pickrem.simulation.actor import Actor
from pickrem.renderer import renderer
from pickrem.physics.physics_engine import PhysicsEngine, FRAME_LENGTH


logger = multiprocessing.get_logger()
# multiprocessing.log_to_stderr(logging.INFO)


def pack(object_message):
    return pickle.dumps(object_message)


def unpack(string_message):
    return pickle.loads(string_message)

# this is a standalone complete message that forces the simulation to kill itself
POISON = pack(None)

# incrementor
_i = -1


def _id():
    global _i
    _i += 1
    return _i

# event messages
PAUSE = _id()
UNPAUSE = _id()

# abstraction of the desired time function in case we want to switch to another
_clock = time.perf_counter

REMOVED_TASK = '<removed-task>'  # placeholder for a removed task

# def SortingList0(list):
# def __lt__(self, rhs):
# return (self[0] < rhs[0])
# def SortingList01(list):
#     def __lt__(self, rhs):
#         return (self[0] < rhs[0]) or (self[0] == rhs[0] and self[1] < rhs[1])

class TaskQueue(object):
    """
    A TaskLoop runs a priority queue of "tasks," functions which when run return the amount of time until they should be
    run next. This allows repetitive tasks to freely control the frequency with which the TaskLoop will attempt to call
    them.
    """

    def __init__(self):
        self.task_queue = queue.PriorityQueue()

        self.entry_finder = {}  # mapping of tasks to entries
        self.counter = itertools.count()  # unique sequence count
        self.get_nowait = self.task_queue.get_nowait
        self.empty = self.task_queue.empty

    def add(self, task, time_until_task=0.0):
        if task in self.entry_finder:
            self.remove(task)
        count = next(self.counter)
        entry = [_clock() + time_until_task, count, task]
        self.entry_finder[task] = entry

        self.task_queue.put_nowait(entry)

    def remove(self, task):
        """
        Mark an existing task as REMOVED_TASK.  Raise KeyError if not found.
        """
        entry = self.entry_finder.pop(task)
        entry[-1] = REMOVED_TASK


class TaskLoop(object):
    def __init__(self):
        self.task_queue = TaskQueue()

    def execute(self):
        assert not self.task_queue.empty()
        self._running = True
        while self._running:
            new_time = _clock()
            task_time, _, task = self.task_queue.get_nowait()
            #print (task_time, task)
            if task_time > new_time:
                #print('sleeping')
                time.sleep(task_time - new_time)
            time_until_repeat = task()
            if time_until_repeat is not None:
                self.task_queue.add(task, time_until_repeat)


class ClientSimulation(TaskLoop):
    """
    This handles the core game logic and things like physics and AI. Each update "frame" generates a message to send
    to the Renderer containing the parts of the game state required for rendering.
    """

    def __init__(self, pipe_connection: PipeConnection):
        super().__init__()
        self.world = Group("World")
        self.actors = Group("Actors")
        self.world.add(self.actors)
        self.pipe_connection = pipe_connection
        self.time_multiplier = 1.0
        self.paused = False
        self.physics = PhysicsEngine(self)
        self.task_manager = TaskLoop()
        self.renderer_callbacks = {}

    def _send_state_to_renderer(self):
        # TODO: refactor something so we're not accessing a protected member
        renderables = {actor.id: actor.get_renderable() for actor in self.actors._entities.values()}
        logger.debug('simulation sending ' + str(len(renderables)) + ' renderables to the renderer')
        message = renderer.pack([renderables, self.renderer_callbacks])
        # print(message)
        self.pipe_connection.send(message)
        self.renderer_callbacks = {}

    def player_controls_handler(self):
        pass

    def _prune_task(self):
        GameEntity.prune()
        gc.collect()

        return 0.5

    def _physics_task(self):
        self.player_controls_handler()
        updated = self.physics.update()
        if updated:
            self._send_state_to_renderer()
        return FRAME_LENGTH

    def _read_messages_task(self):
        """
        process all messages for ClientSimulation in the pipe
        :return:
        """
        while self.pipe_connection.poll(0):
            message = self.pipe_connection.recv()
            logger.debug('received message: ' + str(message))
            if message == POISON:
                self._running = False
                return
            elif message == PAUSE:
                self.paused = True
            elif message == UNPAUSE:
                self.paused = False
            else:
                raise Warning('simulation received unknown message: ' + str(message))
        return 0.004

    def cleanup(self):
        logger.info("shutting down simulation")
        # self.pipe_connection.close()

    def execute(self):
        logger.info("starting up simulation")
        assert (isinstance(actor, Actor) for actor in self.actors)
        assert (isinstance(entity, GameEntity) for entity in self.world)

        # send the initial simulation state to the renderer
        self._send_state_to_renderer()

        # load the task loop
        self.task_queue.add(self._read_messages_task)
        self.task_queue.add(self._physics_task)
        self.task_queue.add(self._prune_task, 5.0)

        # run the task loop
        super().execute()

        self.cleanup()

    def camera_track(self, tracking_target):
        self.renderer_callbacks["tracking_target"] = tracking_target


def run_test(pipe_connection):
    from pickrem.physics.steering_body import SteeringBody
    from pickrem.physics.vec2d import Vec2d
    from pickrem.simulation.player_controller import PlayerController

    simulation = ClientSimulation(pipe_connection)
    test = Actor(
        SteeringBody(position=Vec2d(100, 100)),
        loader.four_way_animations["link"]["walk"],
        "Test Character"
    )
    simulation.actors.add(test)
    player_controller = PlayerController(test)
    simulation.player_controls_handler = player_controller.update
    simulation.camera_track(test.id)

    test = Actor(
        SteeringBody(position=Vec2d(-100, -100)),
        loader.sprites["link"],
        "Test Sprite"
    )
    simulation.actors.add(test)
    simulation.execute()


class ClientSimulatorTests(unittest.TestCase):
    def test_can_run_empty(self):
        outside, sim_side = Pipe()
        # load poison pill before hand to avoid infinite loop
        outside.send(POISON)
        sim = ClientSimulation(sim_side)
        sim.execute()

    def test_sends_messages_empty(self):
        outside, sim_side = Pipe()
        outside.send(POISON)  # load poison pill before hand to avoid infinite loop
        sim = ClientSimulation(sim_side)
        sim.execute()
        self.assertTrue(outside.poll(0.1))

    def test_sends_messages_populated(self):
        outside, sim_side = Pipe()
        outside.send(POISON)  # load poison pill before hand to avoid infinite loop
        run_test(sim_side)
        self.assertTrue(outside.poll(0.1))

    def test_outputs_renderables(self):
        from pickrem.animation.four_way_animation import FourWayAnimation
        from pickrem.animation.sprite import Sprite
        from pickrem.renderer.renderable import Renderable

        # set up pipes and start simulation
        outside, sim_side = Pipe()
        simulation_process = multiprocessing.Process(target=run_test, args=(sim_side,))
        simulation_process.start()

        #  should be plenty of time for the simulation to return something
        self.assertTrue(outside.poll(3.5))

        # receive and unpack the first simulation frame
        first_output = outside.recv()
        print(first_output)
        first_output = unpack(first_output)
        print(first_output)

        # verify the contents of the simulation's output message
        self.assertTrue(isinstance(first_output, list))
        renderables = first_output[0]
        self.assertTrue(isinstance(renderables, dict))
        self.assertEqual(len(renderables), 2)
        renderables = [Renderable(*value) for value in renderables.values()]
        self.assertEqual(renderables[0].position_x, 100)
        self.assertIsInstance(renderables[0].visual, FourWayAnimation)
        self.assertEqual(renderables[1].position_x, -100)
        self.assertIsInstance(renderables[1].visual, Sprite)
        outside.send(POISON)
        simulation_process.join()
