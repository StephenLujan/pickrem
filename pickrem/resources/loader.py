import unittest
import types
import copy
import logging
import pprint
from collections import OrderedDict
# import pdb
#pdb.set_trace()

import pygame

from pickrem.animation.sprite import Sprite
from pickrem.animation.pygame.sprite_sheet import SpriteSheet
from pickrem.animation.animated_sprite import AnimatedSprite
from pickrem.animation.four_way_animation import FourWayAnimation
from pickrem.animation.visual import Visual

FORMAT = '%(levelname)s:%(filename)s:%(lineno)d: %(message)s'
logging.basicConfig(format=FORMAT, level=logging.INFO)
logger = logging.getLogger("loader")

pygame.display.set_mode((1, 1), pygame.HWSURFACE | pygame.DOUBLEBUF)


def _convert_to_table_reference(dictionary, table):
    for key, value in dictionary.items():
        if isinstance(value, dict):
            _convert_to_table_reference(value, table)
        elif isinstance(value, types.LambdaType):
            dictionary[key] = len(table)
            table.append(value)
            # assert(table[dictionary[key]] is value)
    return dictionary


def make_four_way_animation(left, up, right, down):
    return FourWayAnimation(left(), up(), right(), down())


def dict_to_four_way_animation_lambda(dictionary):
    if 'up' in dictionary and 'left' in dictionary and 'right' in dictionary and 'down' in dictionary:
        try:
            assert isinstance(dictionary["left"], types.LambdaType)
            assert isinstance(dictionary["up"], types.LambdaType)
            assert isinstance(dictionary["right"], types.LambdaType)
            assert isinstance(dictionary["down"], types.LambdaType)

            def x():
                logger.debug('dict_to_four_way_animation_lambda(%s)' % str(dictionary))
                return make_four_way_animation(
                    dictionary["left"],
                    dictionary["up"],
                    dictionary["right"],
                    dictionary["down"]
                )

            assert (isinstance(x(), FourWayAnimation))
            return x
        except Exception as e:
            import traceback

            traceback.print_exc()
    return None


def _to_four_way_animations(dictionary):
    assert (isinstance(dictionary, OrderedDict))
    # output = {
    # key: dict_to_four_way_animation_lambda(value) or _to_four_way_animations(value)
    # for key, value in dictionary.items() if isinstance(value, dict)
    # }
    # return output
    output = OrderedDict()
    for key, value in dictionary.items():
        if isinstance(value, dict):
            if 'up' in value and 'left' in value and 'right' in value and 'down' in value:
                logger.debug('converting four way animation dict ' + str(key))
                four_way = dict_to_four_way_animation_lambda(value)
                output[key] = four_way
            else:
                output[key] = _to_four_way_animations(value)
    return output


UP, DOWN, LEFT, RIGHT = "up", "down", "left", "right"
_UNIVERSAL_GRAPHICS_LIST = []


def load(graphics_resource_id):
    # load and possibly create the real Visual we will be rendering
    logger.debug('loading graphics resource #%d', graphics_resource_id)
    resource = _UNIVERSAL_GRAPHICS_LIST[graphics_resource_id]
    logger.debug('loading graphics resource with %s', str(resource))
    if isinstance(resource, types.FunctionType):
        resource = resource()
    assert (isinstance(resource, Visual))
    return resource


# Sprite sheets get reused because they are static. As a low level resource, they are not used outside the animation
# package.
_sprite_sheets = OrderedDict([
    ('link', SpriteSheet("link.png", -1)),
])

_sprites = OrderedDict([
    ('link', lambda: Sprite(_sprite_sheets["link"], (95, 145, 32, 32))),
])
sprites = _convert_to_table_reference(copy.deepcopy(_sprites), _UNIVERSAL_GRAPHICS_LIST)


# sprite animations returns lambdas that can create a new one each time
# this is necessary for each animation to play separately and at different frame rates
_sprite_animations = OrderedDict([
    ("link", OrderedDict([
        ("walk", {
            "right": lambda: AnimatedSprite(_sprite_sheets["link"], (95, 145, 32, 32), 10, True, 10),
            "left": lambda: AnimatedSprite(_sprite_sheets["link"], (95, 145, 32, 32), 10, True, 10,
                                           flip_horizontal=True),
            "up": lambda: AnimatedSprite(_sprite_sheets["link"], (95, 185, 32, 32), 10, True, 10),
            "down": lambda: AnimatedSprite(_sprite_sheets["link"], (95, 105, 32, 32), 10, True, 10)
        }),
        ("attack", {
            "right": lambda: AnimatedSprite(_sprite_sheets["link"], (95, 145, 32, 32), 3, True, 10),
            "left": lambda: AnimatedSprite(_sprite_sheets["link"], (95, 145, 32, 32), 3, True, 10),
            "up": lambda: AnimatedSprite(_sprite_sheets["link"], (95, 145, 32, 32), 3, True, 10),
            "down": lambda: AnimatedSprite(_sprite_sheets["link"], (95, 145, 32, 32), 3, True, 10)
        }),
        ("idle", {
            "right": lambda: AnimatedSprite(_sprite_sheets["link"], (95, 145, 32, 32), 3, True, 10),
            "left": lambda: AnimatedSprite(_sprite_sheets["link"], (95, 145, 32, 32), 3, True, 10),
            "up": lambda: AnimatedSprite(_sprite_sheets["link"], (95, 145, 32, 32), 3, True, 10),
            "down": lambda: AnimatedSprite(_sprite_sheets["link"], (95, 145, 32, 32), 3, True, 10)
        })
    ])),
    ("bogus", {
        "right": lambda: AnimatedSprite(_sprite_sheets["link"], (95, 145, 32, 32), 3, True, 10),
        "left": lambda: AnimatedSprite(_sprite_sheets["link"], (95, 145, 32, 32), 3, True, 10),
        "up": lambda: AnimatedSprite(_sprite_sheets["link"], (95, 145, 32, 32), 3, True, 10),
        "down": lambda: AnimatedSprite(_sprite_sheets["link"], (95, 145, 32, 32), 3, True, 10)
    })
])

# the following are dictionaries resolving to integers which can be used on _UNIVERSAL_GRAPHICS_LIST
sprite_animations = _convert_to_table_reference(copy.deepcopy(_sprite_animations), _UNIVERSAL_GRAPHICS_LIST)

_four_way_animations = _to_four_way_animations(_sprite_animations)
four_way_animations = _convert_to_table_reference(copy.deepcopy(_four_way_animations), _UNIVERSAL_GRAPHICS_LIST)

logger.debug('_sprites:\n' + pprint.pformat(_sprites))
logger.debug('_sprite_animations:\n' + pprint.pformat(_sprite_animations))
logger.debug('_four_way_animations:\n' + pprint.pformat(_four_way_animations))

logger.info('sprites:\n' + pprint.pformat(sprites))
logger.info('sprite_animations:\n' + pprint.pformat(sprite_animations))
logger.info('four_way_animations:\n' + pprint.pformat(four_way_animations))
logger.debug('enumerated _UNIVERSAL_GRAPHICS_LIST:\n' +
             pprint.pformat(list(enumerate(_UNIVERSAL_GRAPHICS_LIST))))


class TestLoadedFourWayAnimations(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        pygame.init()
        cls.display_surface = pygame.display.set_mode((100, 100), pygame.HWSURFACE | pygame.DOUBLEBUF)

    @classmethod
    def tearDownClass(cls):
        cls.display_surface = None
        pygame.quit()

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test_print(self):
        print(_four_way_animations)

    def test_link(self):
        self.assertTrue(isinstance(_four_way_animations["link"]["walk"], types.FunctionType))
        animation = _four_way_animations["link"]["walk"]()
        self.assertTrue(isinstance(animation, FourWayAnimation))
        self.assertTrue(isinstance(animation.up, AnimatedSprite))
        self.assertEqual(_four_way_animations["link"]["walk"], _four_way_animations["link"]["walk"])
        self.assertNotEqual(_four_way_animations["link"]["walk"](), _four_way_animations["link"]["walk"]())
        self.assertTrue(_four_way_animations["link"]["walk"] is
                        _UNIVERSAL_GRAPHICS_LIST[four_way_animations["link"]["walk"]])

    def test_four_way_animations(self):
        logger.info('        test_four_way_animations\n')
        for key, anim in _four_way_animations["link"].items():
            logger.info(key + ' ' + str(anim))
            anim()
        key, anim = 'bogus', _four_way_animations["bogus"]
        logger.info(key + ' ' + str(anim))
        anim()


class TestLoader(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        print('setting up')
        pygame.init()
        cls.display_surface = pygame.display.set_mode((100, 100), pygame.HWSURFACE | pygame.DOUBLEBUF)

    @classmethod
    def tearDownClass(cls):
        cls.display_surface = None
        pygame.quit()

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test_print_all(self):
        print('_sprite_sheets', _sprite_sheets)
        print('_UNIVERSAL_GRAPHICS_LIST', _UNIVERSAL_GRAPHICS_LIST)
        print('_sprite_animations', _sprite_animations)
        print('sprite_animations', sprite_animations)
        print('_four_way_animations', _four_way_animations)
        print('four_way_animations', four_way_animations)

    def test_load_all_resources(self):
        self.assertGreater(len(_UNIVERSAL_GRAPHICS_LIST), 0)
        for i in range(0, len(_UNIVERSAL_GRAPHICS_LIST) - 1):
            visual = load(i)
            self.assertTrue(isinstance(visual, Visual))
        # lets also make sure we can do so twice without issue
        for i in range(0, len(_UNIVERSAL_GRAPHICS_LIST) - 1):
            visual = load(i)
            self.assertTrue(isinstance(visual, Visual))

