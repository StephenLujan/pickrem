import logging

import pygame
from pygame.tests.test_utils import unittest

from pickrem import application
from pickrem.simulation import client_simulation


# logger = multiprocessing.get_logger()
logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger("application")
# logger.setLevel(logging.DEBUG)

class PygameApplication(application.Application):
    """
    This is the top level of the game engine. It manages pygame events (user controls mostly), loads the Renderer, and
    spins off a separate process for the ClientSimulation
    """

    def __init__(self):
        super().__init__(client_simulation.run_test)

    def start(self):
        # start application stuff
        super().start()
        self.clock = pygame.time.Clock()

    def _create_controller(self):
        from pickrem.application_controller import ApplicationController

        return ApplicationController(self)

    def _create_renderer(self, display_surface, pipe_to_simulation):
        from pickrem.renderer.renderer import PygameRenderer

        return PygameRenderer(display_surface, pipe_to_simulation)

    def _create_display_surface(self):
        pygame.init()
        self._display_surface = pygame.display.set_mode(self.window_size, pygame.HWSURFACE | pygame.DOUBLEBUF)
        return self._display_surface

    def resize_window(self, width, height):
        self._display_surface = pygame.display.set_mode((width, height), pygame.HWSURFACE | pygame.DOUBLEBUF)
        self.renderer.camera.surface = self._display_surface

    def cleanup(self):
        super().cleanup()
        pygame.quit()

    def _update(self):
        delta_time = self.clock.tick(application.MAX_FRAME_RATE)
        self.controller.update()
        self.renderer.frame(delta_time)
        pygame.display.flip()


class TestPygameApplication(unittest.TestCase):
    def test_launch(self):
        app = PygameApplication()
        app.start()
        for x in range(50):
            app._update()
        app.cleanup()

    def test_render(self):
        app = PygameApplication()
        app.start()
        self.assertEqual(len(app.renderer.renderables), 0)
        for x in range(50):
            app._update()
        self.assertEqual(len(app.renderer.renderables), 1)
        app.cleanup()


if __name__ == '__main__':
    app = PygameApplication()
    app.execute()