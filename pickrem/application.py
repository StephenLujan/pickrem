import multiprocessing
from abc import ABCMeta, abstractmethod
import logging

from pickrem.simulation import client_simulation





# logger = multiprocessing.get_logger()
logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger("application")
# logger.setLevel(logging.DEBUG)

DEFAULT_RESOLUTION = 640, 400
MAX_FRAME_RATE = 60


class Application(metaclass=ABCMeta):
    """
    This is the top level of the game engine. It manages pygame events (user controls mostly), loads the Renderer, and
    spins off a separate process for the ClientSimulation
    """

    def __init__(self, simulation_run_function):
        """
        :param simulation_run_function: a function which takes a pipe to the renderer as its only argument and in normal
        use will create a pickrem.simulation.client_simulation
        """
        self.window_size = DEFAULT_RESOLUTION
        self._simulation_run_function = simulation_run_function
        self._running = False

    @abstractmethod
    def _create_controller(self):
        pass

    @abstractmethod
    def _create_renderer(self, display_surface, pipe_to_simulation):
        pass

    @abstractmethod
    def _create_display_surface(self):
        pass

    def quit(self):
        self._running = False

    def toggle_paused(self):
        self.paused = not self.paused

    def start(self):
        """
        takes care of initial loading or reloading
        return: Nothing
        """
        logger.info("initializing application")
        logger.debug("launching simulation process")
        # kickoff the simulation process
        self.pipe_to_simulation, self.pipe_to_renderer = multiprocessing.Pipe()
        self.simulation_process = multiprocessing.Process(
            target=self._simulation_run_function, args=(self.pipe_to_renderer,), name='Simulation Process',
            daemon=False)
        self.simulation_process.start()

        logger.debug("creating renderer")
        # we'll host the renderer in this process and thread because pygame isn't thread-safe
        self.renderer = self._create_renderer(self._create_display_surface(), self.pipe_to_simulation)

        logger.debug("creating controller")
        # start the controller in this process and thread again because pygame isn't thread-safe
        self.controller = self._create_controller()

        self._running = True

    @abstractmethod
    def resize_window(self, width, height):
        pass

    @property
    def paused(self):
        return self.renderer.paused

    @paused.setter
    def paused(self, value):
        self.renderer.paused = value
        if value:
            self.pipe_to_simulation.send(client_simulation.PAUSE)
        else:
            self.pipe_to_simulation.send(client_simulation.UNPAUSE)

    def cleanup(self):
        # even if renderer is using the pipe_to_simulation, we're in the same thread so this is safe
        logger.info('Application cleaning up')
        self.pipe_to_simulation.send(client_simulation.POISON)
        self.renderer.cleanup()

        # give the simulation time to close if it hasn't yet, otherwise it may be freak out over its pipe closing.
        self.simulation_process.join(5.0)

    @abstractmethod
    def _update(self):
        pass

    def execute(self):
        self.start()
        assert (self._running)
        while self._running:
            assert (self.simulation_process.is_alive())
            self._update()
        self.cleanup()


class BindableActions():
    QUIT = Application.quit
    TOGGLE_PAUSED = Application.toggle_paused