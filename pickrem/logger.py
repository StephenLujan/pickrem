import multiprocessing
import logging
import os


def get_file_handler(name, formatting="%(asctime)s %(levelname)s - %(message)s", level=logging.DEBUG):
    logging_directory = os.path.join(os.getcwd(), "logs")
    if not os.path.exists(logging_directory):
        os.mkdir(logging_directory)
    path = os.path.join(logging_directory, name + ".log")
    formatter = logging.Formatter(formatting)
    file_handler = logging.FileHandler(path)
    file_handler.setFormatter(formatter)
    file_handler.setLevel(level)
    return file_handler

def get_stream_handler(formatting="%(levelname)s - %(name)s - %(message)s", level=logging.DEBUG):
    formatter = logging.Formatter(formatting)
    stream_handler = logging.StreamHandler()
    stream_handler.setFormatter(formatter)
    stream_handler.setLevel(level)
    return stream_handler

def get_logger(name):
    logger = multiprocessing.get_logger()
    logger.setLevel(logging.DEBUG)
    logger.addHandler(get_file_handler(name))
    logger.addHandler(get_stream_handler())
    return logger