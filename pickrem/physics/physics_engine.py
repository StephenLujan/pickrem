import time


# abstraction of the desired time function in case we want to switch to another
_clock = time.perf_counter

FRAME_LENGTH = 1.0 / 60.0
CAN_GO_FASTER = True


class PhysicsEngine(object):
    """
    PhysicsEngine processes physics for the simulation. PhysicsEngine has its own time tracking to maintain its frames
    at a consistent length if it wants to.
    """

    def __init__(self, simulation):
        self.simulation = simulation  # the simulation
        self.elapsed_time = 0  # the real time elapsed, not sim time
        self.last_time = _clock()  # the last time physics_task was run

    def _step(self, delta_time):
        # TODO: rework the physics to behave more like the physics engines that may be used
        for actor in self.simulation.actors._entities.values():
            actor.body.update(delta_time)

    def update(self):
        """
        runs a number of physics frames dependent on the time past since the last update, and adjusts behavior for the
        simulation time multiplier or pause state

        :return: true if the physics simulation was actually updated
        """
        updated = False
        time = _clock()
        if not self.simulation.paused:
            self.elapsed_time += time - self.last_time
        self.last_time = time

        speed = self.simulation.time_multiplier
        if speed == 1.0 or (self.simulation > 1.0 and not CAN_GO_FASTER):
            frame_length = FRAME_LENGTH
        else:
            frame_length = FRAME_LENGTH * speed

        while self.elapsed_time > frame_length:
            self.elapsed_time -= frame_length
            self._step(frame_length)
            updated = True
        # print (updated)
        return updated

