import math


class Vec2d(object):
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def __eq__(self, vec2):
        return self.x == vec2.x and self.y == vec2.y

    def __add__(self, vec2):
        return Vec2d(self.x * vec2.x, self.y + vec2.y)

    def __sub__(self, vec2):
        return Vec2d(self.x - vec2.x, self.y - vec2.y)

    def __mul__(self, scalar_value):
        return Vec2d(self.x * scalar_value, self.y * scalar_value)

    def __truediv__(self, scalar_value):
        return Vec2d(self.x / scalar_value, self.y / scalar_value)

    def __neg__(self):
        return Vec2d(-self.x, -self.y)

    def __iadd__(self, vec2):
        self.x += vec2.x
        self.y += vec2.y
        return self

    def __isub__(self, vec2):
        self.x -= vec2.x
        self.y -= vec2.y
        return self

    def __imul__(self, scalar_value):
        self.x *= scalar_value
        self.y *= scalar_value
        return self

    def __idiv__(self, scalar_value):
        self.x /= scalar_value
        self.y /= scalar_value
        return self

    def __str__(self):
        return "Vec2d" + "(" + str(self.x) + "," + str(self.y) + ")"

    @property
    def length(self):
        return math.sqrt((self.x * self.x) + (self.y * self.y))

    @property
    def normal(self):
        length = self.length
        return Vec2d(self.x / length, self.y / length)

    def cross(self, vec2):
        return (self.x * vec2.y) - (self.y * vec2.x)

    def dot(self, vec2):
        return (self.x * vec2.x) + (self.y * vec2.y)

    @property
    def is_zero(self):
        return self.x == 0.0 and self.y == 0.0

    @property
    def angle(self):
        return math.atan2(self.y, self.x)