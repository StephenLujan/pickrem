import unittest
import math

from pickrem.physics.self_propelled import SelfPropelled
from pickrem.physics.vec2d import Vec2d


class SteeringBody(SelfPropelled):
    SEEK_TOLERANCE = 1.0

    def __init__(self, mass=1.0, position=None, velocity=None, acceleration=400.0, max_speed=150.0):
        super().__init__(mass, position, velocity, acceleration, max_speed)
        self.controlled = False
        self.seek_goal = None
        self.flee_object = None

    def _seek(self, target_position):
        to_target = target_position - self.position

        distance = to_target.length
        if distance < SteeringBody.SEEK_TOLERANCE:
            return Vec2d(0, 0)
        # use a slightly lower value for distance to create tolerance for the
        # time between successive seek calculations
        max_stoppable_speed = math.sqrt(
            max(0.0, to_target.length - SteeringBody.SEEK_TOLERANCE) * 2.0 * self.acceleration)
        desired_speed = min(self.max_speed, max_stoppable_speed)
        desired_velocity = to_target * (desired_speed / distance)
        return desired_velocity

    def _flee(self, target_position):
        to_target = target_position - self.position
        return -to_target.normal * self.max_speed

    def _get_desired_velocity(self):
        if self.seek_goal is not None:
            desired_velocity = self._seek(self.seek_goal)
        elif self.flee_object is not None:
            desired_velocity = self._flee(self.flee_object)
        else:
            desired_velocity = Vec2d(0, 0)
        return desired_velocity

    def control(self, x, y):
        super().control(x, y)
        self.controlled = True

    def update(self, delta_time):
        # get desired velocity based on our current steering goals
        if not self.controlled:
            self.desired_velocity = self._get_desired_velocity()
        super().update(delta_time)


class SteerableTestCase(unittest.TestCase):
    def test_properties(self):
        steerable = SteeringBody()
        self.assertFalse(steerable.velocity is None)
        self.assertFalse(steerable.position is None)
        self.assertTrue(steerable.force is None)
        self.assertFalse(steerable.resistance is None)
        self.assertFalse(steerable.mass is None)

        self.assertFalse(steerable.max_speed is None)
        self.assertFalse(steerable.max_force is None)
        self.assertTrue(steerable.seek_goal is None)

    def test_seek(self):
        test = SteeringBody()
        test.seek_goal = Vec2d(5, 5)
        for x in range(0, 20):
            test.update(.3)
        self.assertTrue((test.position - Vec2d(5, 5)).length < SteeringBody.SEEK_TOLERANCE)

        test.seek_goal = Vec2d(-5, -5)
        for x in range(0, 30):
            test.update(.3)
        self.assertTrue((test.position - Vec2d(-5, -5)).length < SteeringBody.SEEK_TOLERANCE)

    def test_flee(self):
        test = SteeringBody()
        test.flee_object = Vec2d(5, 5)
        distance = test.flee_object.length
        for x in range(0, 20):
            test.update(.3)
        self.assertTrue((test.position - test.flee_object).length > distance)
