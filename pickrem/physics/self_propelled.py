from pickrem.physics.body import Body
from pickrem.physics.vec2d import Vec2d


class SelfPropelled(Body):
    SEEK_TOLERANCE = 1.0

    def __init__(self, mass: float=1.0, position: Vec2d=None, velocity: Vec2d=None, acceleration: float=1.0,
                 max_speed: float=100.0):

        resistance = 0 if max_speed is None else mass * acceleration / max_speed
        super().__init__(mass, position, velocity, resistance)
        self.max_force = mass * acceleration
        self.desired_velocity = Vec2d(0, 0)

    @property
    def acceleration(self):
        return self.max_force / self.mass

    @acceleration.setter
    def acceleration(self, acceleration):
        self.max_force = acceleration * self.mass

    @property
    def max_speed(self):
        return self.acceleration / self.resistance

    @max_speed.setter
    def max_speed(self, max_speed):
        self.resistance = self.acceleration / max_speed

    def control(self, x, y):
        """
        sets the desired velocity using values [-1,1] that are multipliers of the maximum speed
        :param float x: a value of at least -1 and no more than 1
        :param float y: a value of at least -1 and no more than 1
        :return: Nothing
        """
        assert (1.0 >= x >= -1.0 <= y <= 1.0)
        # TODO: map evenly from a square to a circle? (currently the value 1,1 will yield a desired velocity that is
        # sqrt(2) times greater than max_speed, effectively forcing the update to crop it)
        speed = self.max_speed
        self.desired_velocity = Vec2d(x * speed, y * speed)

    def update(self, delta_time):
        # get desired velocity
        if not self.velocity == self.desired_velocity:

            velocity_difference = self.desired_velocity - self.velocity
            length = velocity_difference.length
            # \current_speed = self.velocity.length
            desired_speed = self.desired_velocity.length

            # if we can match our desired velocity within delta time, do so manually this avoids unnecessary
            # calculations and bouncing back and forth caused by over shooting the velocity goal
            # note: the calculation currently ignores resistance
            if desired_speed < self.max_speed and length < delta_time * self.acceleration and delta_time < 0.1:
                self.velocity = self.desired_velocity
                self.force = None
            else:
                direction = velocity_difference / length
                self.force = direction * self.max_force

        super().update(delta_time)