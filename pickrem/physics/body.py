import unittest

from pickrem.physics.vec2d import Vec2d


class Body(object):
    def __init__(self, mass=1.0, position=None,
                 velocity=None, resistance=0.01):
        if velocity is None:
            velocity = Vec2d(0.0, 0.0)
        if position is None:
            position = Vec2d(0.0, 0.0)
        self.velocity = velocity
        self.position = position
        self.force = None
        self.resistance = resistance
        self.mass = mass

    def update(self, delta_time):
        if not self.force and self.velocity.is_zero:
            return
        if self.force or self.resistance:
            force = Vec2d(0.0, 0.0)
            # resistance force
            if self.resistance:
                force += self.velocity * -self.resistance
            # propulsive force
            if self.force:
                force += self.force
            self.velocity += force * (delta_time / self.mass)
        self.position += self.velocity * delta_time


class BodyTestCase(unittest.TestCase):
    def test_properties(self):
        body = Body()
        self.assertFalse(body.velocity is None)
        self.assertFalse(body.position is None)
        self.assertTrue(body.force is None)
        self.assertFalse(body.resistance is None)
        self.assertFalse(body.mass is None)

    def test_acceleration(self):
        body = Body()
        body.force = Vec2d(-1.0, 0)
        body.mass = 1.0
        body.resistance = None
        body.update(1.0)
        self.assertEqual(body.velocity.x, -1.0)
        self.assertEqual(body.velocity.y, 0.0)
        body.force = Vec2d(1.0, -1.0)
        body.update(2.0)
        self.assertEqual(body.velocity.x, 1.0)
        self.assertEqual(body.velocity.y, -2.0)

    def test_resistance(self):
        body1 = Body()
        body1.force = Vec2d(1.0, 1.0)
        body2 = Body()
        body2.force = Vec2d(-1.0, -1.0)
        control_body = Body()
        control_body.force = Vec2d(1.0, 1.0)
        control_body.resistance = None

        self.assertTrue(body1.resistance > 0.0)
        self.assertTrue(body2.resistance > 0.0)

        for x in range(10):
            body1.update(0.1)
            body2.update(0.1)
            control_body.update(0.1)

        self.assertTrue(body1.position.x < control_body.position.x)
        self.assertTrue(body1.position.y < control_body.position.y)
        self.assertTrue(body1.velocity.x < control_body.velocity.x)
        self.assertTrue(body1.velocity.y < control_body.velocity.y)
        self.assertTrue(body2.position.length < control_body.position.length)
        self.assertTrue(body2.velocity.length < control_body.velocity.length)
