from abc import abstractmethod
import logging

from pickrem.controls.events_template import EventsTemplate





# logger = multiprocessing.get_logger()
logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger("controls")
# logger.setLevel(logging.DEBUG)


class GenericController(EventsTemplate):
    """
    The Controller reacts to external input from the system or user.
    """

    def __init__(self, key_down_handlers={}, key_up_handlers={}, modifier_dependent_key_down_handlers={},
                 modifier_dependent_key_up_handlers={}, joysticks=[]):
        super().__init__()
        # trigger regardless of the state of keys like control and shift
        self.key_down_handlers = key_down_handlers
        logger.debug('key down handlers: %s', key_down_handlers)
        self.key_up_handlers = key_up_handlers
        logger.debug('key up handlers: %s', key_up_handlers)
        self.modifier_dependent_key_down_handlers = modifier_dependent_key_down_handlers
        logger.debug('modifier_dependent_key_down_handlers: %s', modifier_dependent_key_down_handlers)
        self.modifier_dependent_key_up_handlers = modifier_dependent_key_up_handlers
        logger.debug('modifier_dependent_key_up_handlers: %s', modifier_dependent_key_up_handlers)
        self.joysticks = joysticks
        logger.debug('joysticks: %s', joysticks)

    def on_key_down(self, symbol, shift=False, control=False):
        logger.debug("Key %s down.", symbol)
        if symbol in self.key_down_handlers:
            handler = self.key_down_handlers[symbol]
            handler()
        key = (symbol, shift, control)
        if (key in self.modifier_dependent_key_down_handlers):
            handler = self.modifier_dependent_key_down_handlers[symbol]
            handler()

    def on_key_up(self, symbol, shift=False, control=False):
        logger.debug("Key %s up.", symbol)
        if symbol in self.key_up_handlers:
            handler = self.key_up_handlers[symbol]
            handler()

    def on_joy_axis_motion(self, joy_id, axis_id, value):
        self.joysticks[joy_id].on_joy_axis_motion(axis_id, value)

    def on_joybutton_up(self, joy_id, button_number):
        self.joysticks[joy_id].on_joybutton_up(button_number)

    def on_joybutton_down(self, joy_id, button_number):
        self.joysticks[joy_id].on_joybutton_down(button_number)

    def on_joy_hat_motion(self, joy_id, hat_id, x, y):
        self.joysticks[joy_id].on_joy_hat_motion(hat_id, x, y)

    def on_joy_ball_motion(self, joy_id, ball_id, relative_movement):
        self.joysticks[joy_id].on_joy_ball_motion(ball_id, relative_movement)

    @abstractmethod
    def is_key_pressed(self, key):
        pass


class Joystick(object):
    def __init__(self, joystick_id, name, button_down_handlers={}, button_up_handlers={}):
        self.id = joystick_id
        self.name = name
        self.button_down_handlers = button_down_handlers
        self.button_up_handlers = button_up_handlers

    def on_joy_axis_motion(self, axis_id, value):
        logger.debug('Joystick: %s  axis: %i  value: %f', self.name, axis_id, value)

    def on_joybutton_up(self, button_number):
        logger.debug("Joystick: %s  button %i up'", self.name, button_number)
        if button_number in self.button_up_handlers:
            handler = self.button_up_handlers[button_number]
            handler[0](handler[1], handler[2])

    def on_joybutton_down(self, button_number):
        logger.debug("Joystick: %s  button %i down", self.name, button_number)
        if button_number in self.button_down_handlers:
            handler = self.button_down_handlers[button_number]
            handler[0](handler[1], handler[2])

    def on_joy_hat_motion(self, hat_id, x, y):
        logger.debug("Joystick: %s  hat: %i  X: %i  Y: %i", self.name, hat_id, x, y)

    def on_joy_ball_motion(self, ball_id, relative_movement):
        logger.debug("Joystick: %s ball: %i  movement: %f", self.name, ball_id, relative_movement)

    @abstractmethod
    def check_axis(self, axis_id):
        pass

    @abstractmethod
    def check_button(self, button_id):
        pass

    @abstractmethod
    def check_hat(self, hat_id):
        pass