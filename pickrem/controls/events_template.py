from abc import ABCMeta, abstractmethod


class EventsTemplate(metaclass=ABCMeta):
    """
    The generic event handling interface, independent of operating systems and underlying event capturing methods.
    """

    def __init__(self):
        pass

    def on_input_focus(self):
        pass

    def on_input_blur(self):
        pass

    def on_key_down(self, symbol, shift=False, control=False):
        pass

    def on_key_up(self, symbol, shift=False, control=False):
        pass

    def on_mouse_focus(self):
        pass

    def on_mouse_blur(self):
        pass

    def on_mouse_move(self, position, relative_movement, buttons):
        pass

    def on_mouse_wheel_roll_up(self, x, y):
        pass

    def on_mouse_wheel_roll_down(self, x, y):
        pass

    def on_mouse_lbutton_up(self, x, y):
        pass

    def on_mouse_lbutton_down(self, x, y):
        pass

    def on_mouse_rbutton_up(self, x, y):
        pass

    def on_mouse_rbutton_down(self, x, y):
        pass

    def on_mouse_mbutton_up(self, x, y):
        pass

    def on_mouse_mbutton_down(self, x, y):
        pass

    def on_minimize(self):
        pass

    def on_restore(self):
        pass

    def on_resize(self, width, height):
        pass

    def on_expose(self):
        pass

    def on_exit(self):
        pass

    def on_user(self, event):
        pass

    def on_joy_axis_motion(self, joy_id, axis_id, value):
        pass

    def on_joybutton_up(self, joy_id, button_number):
        pass

    def on_joybutton_down(self, joy_id, button_number):
        pass

    def on_joy_hat_motion(self, joy_id, hat_id, x, y):
        pass

    def on_joy_ball_motion(self, joy_id, ball_id, relative_movement):
        pass

    @abstractmethod
    def on_event(self, event):
        """
        This simply converts and routes each event into the engine agnostic methods of EventsTemplate.
        :param event: the form of event is not known in EventsTemplate
        """

    @abstractmethod
    def update(self):
        """
        Process all events
        """
