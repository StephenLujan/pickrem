import logging

import pygame
from pygame.constants import *

from pickrem.controls.generic_controller import GenericController, Joystick


# logger = multiprocessing.get_logger()
logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger("controls")
# logger.setLevel(logging.DEBUG)


class PygameController(GenericController):
    def __init__(self, key_down_handlers={}, key_up_handlers={}, modifier_dependent_key_down_handlers={},
                 modifier_dependent_key_up_handlers={}):
        # ensure pygame is already initialized
        pygame.init()
        super().__init__(key_down_handlers, key_up_handlers, modifier_dependent_key_down_handlers,
                         modifier_dependent_key_up_handlers, PygameJoystick.load_all())

    def update(self):
        """processes all events"""
        for event in pygame.event.get():
            self.on_event(event)

    def on_event(self, event):
        if event.type == QUIT:
            logger.debug('quit event received')
            self.on_exit()

        elif event.type >= USEREVENT:
            self.on_user(event)

        elif event.type == VIDEOEXPOSE:
            self.on_expose()

        elif event.type == VIDEORESIZE:
            logger.debug('resizing to %i x %i', event.w, event.h)
            self.on_resize(event.w, event.h)

        elif event.type == KEYUP:
            self.on_key_up(event.key, event.mod & KMOD_SHIFT, event.mod & KMOD_CTRL)

        elif event.type == KEYDOWN:
            self.on_key_down(event.key, event.mod & KMOD_SHIFT, event.mod & KMOD_CTRL)

        elif event.type == MOUSEMOTION:
            self.on_mouse_move(event.pos, event.rel, event.buttons)

        elif event.type == MOUSEBUTTONUP:
            x, y = event.pos
            if event.button == 1:
                self.on_mouse_lbutton_up(x, y)
            elif event.button == 2:
                self.on_mouse_mbutton_up(x, y)
            elif event.button == 3:
                self.on_mouse_rbutton_up(x, y)

        elif event.type == MOUSEBUTTONDOWN:
            x, y = event.pos
            if event.button == 1:
                self.on_mouse_lbutton_down(x, y)
            elif event.button == 2:
                self.on_mouse_mbutton_down(x, y)
            elif event.button == 3:
                self.on_mouse_rbutton_down(x, y)
            elif event.button == 4:
                self.on_mouse_wheel_roll_up(x, y)
            elif event.button == 5:
                self.on_mouse_wheel_roll_down(x, y)

        elif event.type == ACTIVEEVENT:
            if event.state == 1:
                if event.gain:
                    self.on_mouse_focus()
                else:
                    self.on_mouse_blur()
            elif event.state == 2:
                if event.gain:
                    self.on_input_focus()
                else:
                    self.on_input_blur()
            elif event.state == 4:
                if event.gain:
                    self.on_restore()
                else:
                    self.on_minimize()

        elif event.type == JOYHATMOTION:
            self.on_joy_hat_motion(event.joy, event.hat, event.value[0], event.value[1])

        elif event.type == JOYBUTTONUP:
            self.on_joybutton_up(event.joy, event.button)

        elif event.type == JOYBUTTONDOWN:
            self.on_joybutton_down(event.joy, event.button)

        elif event.type == JOYBALLMOTION:
            self.on_joy_ball_motion(event.joy, event.ball, event.rel)

        elif event.type == JOYAXISMOTION:
            self.on_joy_axis_motion(event.joy, event.axis, event.value)

    def is_key_pressed(self, key):
        return pygame.key.get_pressed()[key]


class PygameJoystick(Joystick):
    @staticmethod
    def load_all():
        return [PygameJoystick(joystick_id) for joystick_id in range(pygame.joystick.get_count())]

    def __init__(self, joystick_id):
        logger.debug("Initializing joystick %i'", joystick_id)
        self.pygame_joystick = pygame.joystick.Joystick(joystick_id)
        self.pygame_joystick.init()
        super().__init__(joystick_id, self.pygame_joystick.get_name())
        logger.info("Initialized joystick '%s'", self.name)

    def check_axis(self, axis_id):
        return self.pygame_joystick.get_axis(axis_id)

    def check_button(self, button_id):
        return self.pygame_joystick.get_button(button_id)

    def check_hat(self, hat_id):
        return self.pygame_joystick.get_hat(hat_id)
