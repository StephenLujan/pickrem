import logging

from pygame.constants import *

from pickrem.controls.pygame_controller import PygameController as GameEngineController
from pickrem.application import BindableActions as App


# logger = multiprocessing.get_logger()
logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger("controls")
# logger.setLevel(logging.DEBUG)


class ApplicationController(GameEngineController):
    """
    The Controller reacts to external input from the system or user.
    """

    def __init__(self, application):
        self.application = application
        # trigger regardless of the state of keys like control and shift
        key_down_handlers = {
            K_q: lambda: App.QUIT(self.application),
            K_ESCAPE: lambda: App.QUIT(self.application),
            K_p: lambda: App.TOGGLE_PAUSED(self.application)
        }
        super().__init__(key_down_handlers=key_down_handlers)

    def on_resize(self, width, height):
        # the window itself may already be resized but the application states don't yet reflect the change so...
        self.application.resize_window(width, height)

    def on_exit(self):
        App.QUIT(self.application)