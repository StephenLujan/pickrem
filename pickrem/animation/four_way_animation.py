from unittest import TestCase

from pickrem.animation.animated_sprite import AnimatedSprite
from pickrem.animation.pygame.sprite_sheet import SpriteSheet
from pickrem.animation.visual import Visual

LEFT = "left"
UP = "up"
RIGHT = "right"
DOWN = "down"


class FourWayAnimation(Visual):
    """
    An animation that plays in any of four directions
    """

    def __init__(self, left: AnimatedSprite, up: AnimatedSprite, right: AnimatedSprite, down: AnimatedSprite):
        """
        4 sprite animations assumed to be the same temporal length
        """
        super().__init__()
        self.left = left
        self.up = up
        self.right = right
        self.down = down

        self._current_animation = self.right
        self._current_direction = RIGHT

    @property
    def direction(self):
        return self._current_direction

    @direction.setter
    def direction(self, direction):
        # print(direction)
        time = self._current_animation.time_into_animation
        frame = self._current_animation.frame_number
        if direction == RIGHT:
            self._current_direction = direction
            self._current_animation = self.right
        elif direction == LEFT:
            self._current_direction = direction
            self._current_animation = self.left
        elif direction == UP:
            self._current_direction = direction
            self._current_animation = self.up
        elif direction == DOWN:
            self._current_direction = direction
            self._current_animation = self.down
        self._current_animation.time_into_animation = time
        self._current_animation.frame_number = frame

    def update(self, delta_time):
        """Returns the current sprites after you pass the delta time in milliseconds."""
        self._current_animation.update(delta_time)

    def blit(self, cam, position_x, position_y, scale_x=1.0, scale_y=1.0, rotation=0):
        # switch directions based on the difference between our world rotation and the camera rotation
        rotation -= cam.rotation
        rotation %= 360
        if rotation >= 315.0 or rotation < 45.0:
            self.direction = RIGHT
        elif 45.0 <= rotation < 135.0:
            self.direction = UP
        elif 135.0 <= rotation < 225.0:
            self.direction = LEFT
        elif 225.0 <= rotation < 315.0:
            self.direction = DOWN

        # Pass this work along to the underlying AnimatedSprite, but always rotate with the camera, because we just want
        # rotation to switch animations, not actually rotate the image
        self._current_animation.blit(cam, position_x, position_y, scale_x, scale_y, None)

    # we have no actions
    @property
    def action(self):
        return None

    # we have no actions
    @action.setter
    def action(self, action):
        pass


class TestFourWayAnimation(TestCase):
    @classmethod
    def setUpClass(cls):
        import pygame
        from pickrem.animation.pygame.camera import Camera

        pygame.init()
        display_surface = pygame.display.set_mode((100, 100), pygame.HWSURFACE | pygame.DOUBLEBUF)
        cls.DEFAULT_CAMERA = Camera(display_surface, 0, 0)

    @classmethod
    def tearDownClass(cls):
        import pygame

        pygame.quit()

    def setUp(self):
        self.sprite_sheet = SpriteSheet('link.png')
        self.animation = FourWayAnimation(
            AnimatedSprite(self.sprite_sheet, (95, 145, 32, 32), 10, True, 10),
            AnimatedSprite(self.sprite_sheet, (95, 155, 32, 32), 10, True, 10),
            AnimatedSprite(self.sprite_sheet, (95, 165, 32, 32), 10, True, 10),
            AnimatedSprite(self.sprite_sheet, (95, 175, 32, 32), 10, True, 10),
        )

    def tearDown(self):
        pass

    def test_can_build(self):
        self.assertIsInstance(self.animation, FourWayAnimation)

    def test_action(self):
        print('test action: ', self.animation.action)
        self.animation.action = "test"

    def test_can_blit(self):
        cam = self.DEFAULT_CAMERA
        self.animation.blit(cam, 1, 2)
        self.animation.blit(cam, 2, 3, rotation=180)
        self.animation.blit(cam, 3, 4, scale_x=2.0, scale_y=2.0)
        self.animation.blit(cam, 4, 5, scale_x=2.0, scale_y=2.0, rotation=180)
