import os
from abc import abstractmethod, ABCMeta

import pickrem


path = os.path.abspath(pickrem.__file__)
dir_path = os.path.dirname(path)
SPRITE_PATH = dir_path + "\\resources\\"
# print("sprite path: " + SPRITE_PATH)


class SpriteSheet(metaclass=ABCMeta):
    """
    This object handles a sprite sheet which may be used for many sprites or animations
    """

    def __init__(self, filename, use_alpha=False, transparency_color=None):
        assert (use_alpha == True if transparency_color is not None else True)
        # Cache of images loaded from sheet
        self._image_cache = {}
        # Cache of strips loaded from sheet. The value images are not duplicated here but shared with _image_cache
        # so the only additional memory is the dictionary itself
        self._strip_cache = {}
        self.transparency_color = transparency_color

    @abstractmethod
    def image_at(self, rectangle, flip_horizontal=False, flip_vertical=False):
        """
        Loads a specific image from a specific rectangle
        When calling images_at the rect is the format: (x, y, x_size, y_size)
        Note that all results are cached.

        :param rectangle: tuple(x, y, x_size, y_size)
        :rtype : sprites
        """
        pass

    def _images_at(self, rects, flip_horizontal=False, flip_vertical=False):
        """
        Loads multiple images and return them as a list. The results are not cached; use load_strip for this.

        :param rects: list[tuple(x, y, x_size, y_size)]
        :rtype : list[sprites]
        """
        return [self.image_at(rect, flip_horizontal, flip_vertical) for rect in rects]

    def load_strip(self, rect, image_count, flip_horizontal=False, flip_vertical=False):
        """
        Loads a contiguous left to right strip of images and returns them as a list. All results are cached.

        :param rect: tuple(x, y, x_size, y_size)
        :param image_count: integer > 0
        :rtype : list[sprites]
        """
        key = (rect, image_count, flip_horizontal, flip_vertical)
        if key in self._strip_cache:
            return self._strip_cache[key]

        tuples = [(rect[0] + rect[2] * x, rect[1], rect[2], rect[3]) for x in range(image_count)]
        images = self._images_at(tuples, flip_horizontal, flip_vertical)
        self._strip_cache[key] = images
        return images