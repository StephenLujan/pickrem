from abc import abstractmethod, ABCMeta


class Visual(metaclass=ABCMeta):
    """
    Visual is the base class for every kind of discrete renderable animated or non animated image
    """

    def __init__(self):
        self.action = None

    @abstractmethod
    def blit(self, cam, position_x, position_y, scale_x=1.0, scale_y=1.0, rotation=0):
        """
        draws this Visual at the proper screen location and transforms for the supplied camera
        :param position_x: the world position x coordinate to render at
        :param position_y: the world position y coordinate to render at
        :param scale_x: a float multiplier of the y scale of this visual relative to the rest of the world
        :param scale_y: a float multiplier of the x scale of this visual relative to the rest of the world
        :param rotation: rotation in degrees clockwise relative to the world
        :param cam: the camera that may render this Visual
        :return: Nothing
        """
        pass

    @abstractmethod
    def update(self, delta_time):
        pass

    @property
    @abstractmethod
    def action(self):
        """
        :return: a string representing the current action being animated or None
        """
        pass

    @action.setter
    @abstractmethod
    def action(self, action: str):
        """
        For visuals with multiple animations or states this will switch to or invoke the specified animation
        :param action: a string identifying the action to be animated or None
        """
        pass


