import pygame

import pickrem.animation.camera


class Camera(pickrem.animation.camera.Camera):
    def __init__(self, surface: pygame.SurfaceType, x: float=0.0, y: float=0.0, zoom: float=1.0, rotation: float=0.0):
        assert (isinstance(surface, pygame.SurfaceType))
        self._surface = surface
        super().__init__(surface.get_width() / 2, surface.get_height() / 2, x, y, zoom, rotation)

    @property
    def surface(self):
        return self._surface

    @surface.setter
    def surface(self, surface: pygame.SurfaceType):
        self._surface = surface
        self.horizontal_offset = surface.get_width() / 2
        self.vertical_offset = surface.get_height() / 2