import pygame


class CachingImage(object):
    """
    This maintains an unmodified original image, but has the capability of caching the results of transformations like
    rotations and scaling. This is a lower level class that also helps abstract pygame specific code out of the Visuals.
    """

    def __init__(self, image: pygame.Surface):
        self._image = image
        self._width = image.get_width()
        self._height = image.get_height()
        self._scaled_image = image
        self._final_image = image
        self._scale_x = 1.0
        self._scale_y = 1.0
        self._rotation = 0

    def blit(self, camera, x, y, scale_x=1.0, scale_y=1.0, rotation=0):
        """
        blits an image to a surface, with proper position and transforms

        :param camera: a camera is needed to convert world coordinates to screen coordinates
        :param x: the x world coordinates to draw to
        :param y: the y world coordinates to draw to
        :param scale_x: float multiplier for the scale of the image
        :param scale_y: float multiplier for the scale of the image
        :param rotation: clockwise degrees
        :return: nothing
        """
        # offset scale and rotation for camera zoom and rotation
        scale_x *= camera.zoom
        scale_y *= camera.zoom
        if rotation is None:
            rotation = 0
        else:
            rotation -= camera.rotation

        # Scale then rotate as needed. Thus rotation changes may be performed separately, while scale changes require
        # both a scale and a rotate operation. The order was chosen on the basis that when rotation changes are
        # occurring, they will occur more frequently than scale changes.
        if scale_x != self._scale_x or scale_y != self._scale_y:
            self._scale_x = scale_x
            self._scale_y = scale_y
            self._width = int(self._image.get_width() * scale_x)
            self._height = int(self._image.get_height() * scale_y)
            self._scaled_image = pygame.transform.smoothscale(self._image, (self._width, self._height))
            if rotation == 0.0:
                self._final_image = self._scaled_image
            else:
                self._final_image = pygame.transform.rotate(self._scaled_image, rotation)
        if rotation != self._rotation:
            self._rotation = rotation
            if rotation == 0.0:
                self._final_image = self._scaled_image
            else:
                self._final_image = pygame.transform.rotate(self._scaled_image, rotation)
        x, y = camera.world_to_screen(x, y)
        x -= self._width / 2
        y -= self._height / 2
        camera.surface.blit(self._final_image, (x, y, self._width, self._height))
