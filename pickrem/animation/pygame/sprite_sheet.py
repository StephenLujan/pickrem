import pygame

import pickrem
import pickrem.animation.sprite_sheet

SPRITE_PATH = pickrem.animation.sprite_sheet.SPRITE_PATH
CACHE = {}


class SpriteSheet(pickrem.animation.sprite_sheet.SpriteSheet):
    """
    This object handles a sprite sheet which may be used for many sprites or animations, and is rather pygame specific.
    """

    def __init__(self, filename, use_alpha=False, transparency_color=None):
        super().__init__(filename, use_alpha, transparency_color)
        if filename in CACHE:
            self.sheet = CACHE[filename]
        else:
            try:
                self.sheet = pygame.image.load(SPRITE_PATH + filename)
                if use_alpha:
                    self.sheet = self.sheet.convert_alpha()
                else:
                    self.sheet = self.sheet.convert()
                CACHE[filename] = self.sheet
            except pygame.error as message:
                print('Unable to load spritesheet sprites:', SPRITE_PATH + filename)
                raise SystemExit(message)

        if transparency_color is None and use_alpha:
            self.transparency_color = self.sheet.get_at((0, 0))

    def image_at(self, rectangle, flip_horizontal=False, flip_vertical=False):
        """
        Loads a specific image from a specific rectangle
        When calling images_at the rect is the format: (x, y, x_size, y_size)
        Note that all results are cached.

        :param rectangle: tuple(x, y, x_size, y_size)
        :rtype : sprites
        """
        key = (rectangle, flip_horizontal, flip_vertical)
        if key in self._image_cache:
            return self._image_cache[key]

        rect = pygame.Rect(rectangle)
        image = pygame.Surface(rect.size)
        image.blit(self.sheet, (0, 0), rect)
        if self.transparency_color is not None:
            image.set_colorkey(self.transparency_color, pygame.RLEACCEL)

        if flip_horizontal or flip_vertical:
            image = pygame.transform.flip(image, flip_horizontal, flip_vertical)

        self._image_cache[key] = image
        return image