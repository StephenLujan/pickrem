from unittest import TestCase, skipIf
import os

from pickrem.animation.pygame.caching_image import CachingImage
from pickrem.animation.visual import Visual
from pickrem.animation.pygame.sprite_sheet import SpriteSheet


class Sprite(Visual):
    """
    This is a simple non-animated sprite, with some abstraction away from renderer specific code.
    """

    def __init__(self, sprite_sheet: SpriteSheet, frame_rectangle):
        """
        construct a Sprite

        :param sprite_sheet: the SpriteSheet to load the animation from
        :param frame_rectangle: the top left x y followed by x y dimensions
        """
        super().__init__()
        self.sprite_sheet = sprite_sheet
        self.image = CachingImage(sprite_sheet.image_at(frame_rectangle))

    def blit(self, cam, position_x, position_y, scale_x=1.0, scale_y=1.0, rotation=0):
        self.image.blit(cam, position_x, position_y, scale_x, scale_y, rotation)

    # does not change at all
    def update(self, delta_time):
        pass

    # we have no actions
    @property
    def action(self):
        return None

    # we have no actions
    @action.setter
    def action(self, action):
        pass


class TestSprite(TestCase):
    @classmethod
    def setUpClass(cls):
        import pygame
        from pickrem.animation.pygame.camera import Camera

        pygame.init()
        display_surface = pygame.display.set_mode((600, 450), pygame.HWSURFACE | pygame.DOUBLEBUF)
        cls.DEFAULT_CAMERA = Camera(display_surface, 0, 0)


    @classmethod
    def tearDownClass(cls):
        import pygame

        pygame.quit()

    def setUp(self):
        self.sprite_sheet = SpriteSheet('link.png')
        self.sprite = Sprite(self.sprite_sheet, (95, 145, 32, 32))

    def tearDown(self):
        pass

    def test_can_build(self):
        self.assertIsInstance(self.sprite, Sprite)

    def test_action(self):
        print(self.sprite.action)
        self.sprite.action = "test"

    def test_can_blit(self):
        cam = self.DEFAULT_CAMERA
        self.sprite.blit(cam, 1, 2)
        self.sprite.blit(cam, 2, 3, rotation=180)
        self.sprite.blit(cam, 3, 4, scale_x=2.0, scale_y=2.0)
        self.sprite.blit(cam, 4, 5, scale_x=2.0, scale_y=2.0, rotation=180)

    @skipIf(os.environ.get('RUNALL'), "manually validated test")
    def test_eyes_on(self):
        import pygame
        import time

        cam = self.DEFAULT_CAMERA
        self.sprite.blit(cam, -100, -100)
        self.sprite.blit(cam, 100, -100, rotation=180)
        self.sprite.blit(cam, 100, 100, scale_x=2.0, scale_y=2.0)
        self.sprite.blit(cam, -100, 100, scale_x=2.0, scale_y=2.0, rotation=180)
        pygame.display.flip()
        for x in range(100):
            pygame.event.pump()
            time.sleep(.10)
