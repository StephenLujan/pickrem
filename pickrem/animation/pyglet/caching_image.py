class CachingImage(object):
    def __init__(self, texture):
        self.texture = texture
        self._width = texture.width
        self._height = texture.height
        self._scale_x = 1.0
        self._scale_y = 1.0
        self._rotation = 0
        # TODO: set anchor

    def blit(self, camera, x, y, scale_x=1.0, scale_y=1.0, rotation=0):
        """
        blits an image to a surface, with proper position and transforms

        :param camera: a camera is needed to convert world coordinates to screen coordinates
        :param x: the x world coordinates to draw to
        :param y: the y world coordinates to draw to
        :param scale_x: float multiplier for the scale of the image
        :param scale_y: float multiplier for the scale of the image
        :param rotation: clockwise degrees
        :return: nothing
        """
        assert scale_y == scale_x  # TODO: figure out pyglet scaling by component vector

        # offset scale and rotation for camera zoom and rotation
        scale_x *= camera.zoom
        scale_y *= camera.zoom
        if rotation is None:
            rotation = 0
        else:
            rotation -= camera.rotation
        assert rotation % 90 == 0  # TODO: figure out pyglet rotation for arbitrary angles

        x, y = camera.world_to_screen(x, y)
        z = (y + 5000.0) / 10000.0  # z must be between 0.0 and 1.0 to actually render
        self.texture.blit(x, y, z)
