import unittest
import math

WORLD_UNIT_TO_PIXEL = 1.0


class Camera(object):
    """
    A virtual camera capable of panning, rotating, and zooming. Responsible for converting from world coordinates to
    screen coordinates.
    """

    def __init__(self, horizontal_offset, vertical_offset, x: float=0.0, y: float=0.0, zoom: float=1.0,
                 rotation: float=0.0, invert_y=True):
        """
        :param horizontal_offset: the horizontal distance from the screen's edge to the camera point; generally half of
            the screen's size
        :param vertical_offset: the vertical distance from the screen's edge to the camera point; generally half of the
            screen's size
        :param x: starting world x-coordinate of the camera
        :param y: starting world y-coordinate of the camera
        :param zoom: multiplier for display size of everything
        :param rotation:
        :param invert_y: The screen space for most renderers starts at the top left, down and to the right are positive.
            Positive y being down is kind of counter-intuitive so we flip it by default
        :return:
        """
        assert (zoom > 0)
        self.horizontal_offset = horizontal_offset
        self.vertical_offset = vertical_offset
        self.zoom = float(zoom)
        self.rotation = rotation
        self.x = x  # world x position of camera
        self.y = y  # world y position of camera
        self.invert_y = -1.0 if invert_y else 1.0
        self.tracking_target = None  # camera will automatically pan to tracking target every update

    def world_to_screen(self, world_x, world_y):
        """
        convert from the game world coordinates to on-screen coordinates used for blitting
        :param world_x: world x-coordinate
        :param world_y: world y-coordinate
        :return: screen x coordinate, screen y coordinate
        """
        # vector in world units relative to camera
        x = world_x - self.x
        y = world_y - self.y

        # let's make positive y up in world space even though it's down in screen space
        y *= self.invert_y

        # camera rotation
        radians = math.radians(-self.rotation)
        x, y = x * math.cos(radians) - y * math.sin(radians), x * math.sin(radians) + y * math.cos(radians)

        # convert from world units to pixels, then center the 0 vector on the screen
        x = x * self.zoom * WORLD_UNIT_TO_PIXEL + self.horizontal_offset
        y = y * self.zoom * WORLD_UNIT_TO_PIXEL + self.vertical_offset

        return x, y

    def track(self, renderable):
        self.x = renderable.position_x
        self.y = renderable.position_y
        self.tracking_target = renderable

    def update(self, delta_time):
        if self.tracking_target is not None:
            self.x = self.tracking_target.position_x
            self.y = self.tracking_target.position_y


class TestCamera(unittest.TestCase):
    def setUp(self):
        self.cam = Camera(horizontal_offset=400, vertical_offset=300, x=100.0, y=100.0)

    def test_world_to_screen_unzoomed(self):
        # +10,+10 from the cam centered on 100,100
        x, y = self.cam.world_to_screen(110, 110)
        self.assertEqual(x, 410)
        self.assertEqual(y, 290)

        #-10,-10 from the cam centered at -100,-100
        self.cam.x = -100
        self.cam.y = -100
        x, y = self.cam.world_to_screen(-110, -110)
        self.assertEqual(x, 390)
        self.assertEqual(y, 310)

    def test_world_to_screen_zoomed(self):
        self.cam.zoom = 2.0
        x, y = self.cam.world_to_screen(110, 110)
        self.assertEqual(x, 420)
        self.assertEqual(y, 280)
        self.cam.x = -100
        self.cam.y = -100
        x, y = self.cam.world_to_screen(-110, -110)
        self.assertEqual(x, 380)
        self.assertEqual(y, 320)

    def test_world_to_screen_rotated(self):
        x, y = self.cam.world_to_screen(110, 110)
        self.assertEqual(x, 410)
        self.assertEqual(y, 290)
        self.cam.rotation = 180.0
        x, y = self.cam.world_to_screen(110, 110)
        self.assertEqual(x, 390)
        self.assertEqual(y, 310)