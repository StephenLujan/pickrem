from unittest import TestCase

from pickrem.animation.pygame.sprite_sheet import SpriteSheet
from pickrem.animation.pygame.caching_image import CachingImage
from pickrem.animation.visual import Visual


class AnimatedSprite(Visual):
    """
    sprite strip animator
    """

    def __init__(self, sprite_sheet: SpriteSheet, first_frame_rectangle, count, loop=False, frames_per_second=16.0,
                 flip_horizontal=False, flip_vertical=False):
        """
        construct an AnimatedSprite

        first_frame_rectangle and count are the same arguments used by spritesheet.load_strip.

        :param sprite_sheet: the SpriteSheet to load the animation from
        :param first_frame_rectangle: the top left x y followed by x y dimensions
        :param count: the number of consecutive keys
        :param loop: a boolean that, when True, causes the sequence to repeat
        :param frames_per_second: the default animation play back speed
        """
        super().__init__()
        self.sprite_sheet = sprite_sheet
        self.images = [CachingImage(image) for image in sprite_sheet.load_strip(first_frame_rectangle, count,
                                                                                flip_horizontal, flip_vertical)]
        self.frame_number = 0
        self.current_image = self.images[self.frame_number]
        self.loop = loop
        self.frame_length = 1000.0 / float(frames_per_second)  # in milliseconds
        self.time_into_animation = 0.0

    def reset(self):
        """
        resets an animation to the first frame
        """
        self.time_into_animation = 0
        self.frame_number = 0

    def update(self, delta_time):
        """
        Returns the current sprites after you pass the delta time in milliseconds.
        """
        # if we aren't looping and the frame number is anywhere at or past the final frame, return the final frame
        # this is an optimization so that we don't continue frame calculations after we're on the final frame
        if (not self.loop) and delta_time > 0 and self.frame_number >= len(self.images):
            self.current_image = self.images[-1]
            return self.current_image
        # advance the time into the animation by the amount of time passed
        self.time_into_animation += delta_time
        # advance the animation to the next frame if enough time has accumulated to exceed frame length
        # also keep in mind we may have suddenly accumulated enough time with deltaTime to pass several frames
        self.frame_number = int(self.time_into_animation // self.frame_length)
        self.time_into_animation %= self.frame_length * len(self.images)
        # If we're looping the animation the modular division should always yield the correct frame in the sequence
        if self.loop:
            self.frame_number %= len(self.images)
        # if we aren't looping and the frame number is anywhere at or past the final frame, return the final frame
        elif self.frame_number >= len(self.images):
            self.current_image = self.images[-1]
            return self.current_image
        self.current_image = self.images[self.frame_number]
        return self.current_image

    def blit(self, cam, position_x, position_y, scale_x=1.0, scale_y=1.0, rotation=0):
        self.current_image.blit(cam, position_x, position_y, scale_x, scale_y, rotation)

    def __add__(self, sprite_sheet):
        self.images.extend([CachingImage(image) for image in sprite_sheet.images])
        assert isinstance(self, object)
        return self

    # we have no actions
    @property
    def action(self):
        return None

    # we have no actions
    @action.setter
    def action(self, action):
        pass


class TestSpriteAnimation(TestCase):
    @classmethod
    def setUpClass(cls):
        import pygame
        from pickrem.animation.pygame.camera import Camera

        pygame.init()
        display_surface = pygame.display.set_mode((600, 450), pygame.HWSURFACE | pygame.DOUBLEBUF)
        cls.DEFAULT_CAMERA = Camera(display_surface, 0, 0)

    @classmethod
    def tearDownClass(cls):
        import pygame

        pygame.quit()

    def setUp(self):
        self.animation = AnimatedSprite(SpriteSheet('link.png'), (10, 10, 20, 20), 10, True, 10)

    def tearDown(self):
        pass

    def test_play_looping(self):
        animation = self.animation
        animation.update(150)
        animation.update(150)
        animation.update(150)
        self.assertEquals(animation.frame_number, 4)
        animation.update(99000)
        self.assertEquals(animation.frame_number, 4)
        animation.update(-200)
        self.assertEquals(animation.frame_number, 2)
        animation.update(-99100)
        self.assertEquals(animation.frame_number, 1)

    def test_action(self):
        print(self.animation.action)
        self.animation.action = "test"

    def test_can_blit(self):
        cam = self.DEFAULT_CAMERA
        self.animation.blit(cam, 1, 2)
        self.animation.blit(cam, 2, 3, rotation=180)
        self.animation.blit(cam, 3, 4, scale_x=2.0, scale_y=2.0)
        self.animation.blit(cam, 4, 5, scale_x=2.0, scale_y=2.0, rotation=180)