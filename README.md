# README #

This is a 2D game engine built overtop Pygame, with the immediate goal of making basic widespread game needs much easier to implement. The project is at an early stage, and some features may be partially implemented. There is some abstraction of Pygame specific code away from the rest of the engine, and support of other low level engines like Pyglet or PySDL2 is under consideration.

## Current Major Features ##

* A multi-process architecture that separates the rendering and simulation processes (which will make networking easier in the future)

* Rendering based on a virtual camera with pan, zoom, and rotate

* An Animation package with sprite sheets, animated sprites, and rotation and scaling transform caching

* A controller which makes the addition of handlers for user and system input much simpler, and abstracts the pygame specific event-based architecture.

* A resource loader that makes it simple to create objects from the Animation package, and avoids unnecessary in memory duplication.