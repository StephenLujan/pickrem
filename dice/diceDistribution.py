import matplotlib

import dice


matplotlib.rcParams['backend'] = 'QT4AGG'
from matplotlib.pyplot import *
from numpy import linspace
from scipy.stats import *

x = linspace(0, 1, 75)
die = dice.DiceMax10Wide()

fig = figure()
ax = fig.add_subplot(111)
rolls = [die.roll(5) for r in range(10000)]
ax.hist(rolls, 200, color='c')
median = nanmedian(rolls)
print("median: " + str(median))
mean = nanmean(rolls)
print("mean: " + str(mean))
ax.axvline(median, color='m', linestyle='dashed', linewidth=2, label="median")
ax.axvline(mean, color='r', linestyle='dashed', linewidth=2, label="mean")
ax.legend(bbox_to_anchor=(0., 1.02, 1., .102), loc=3,
          ncol=2, mode="expand", borderaxespad=0.)
show()
# fig.savefig("Beta_distribution_pdf.pdf",bbox_inches="tight",pad_inches=.15)

'''
fig = figure()
ax = fig.add_subplot(111)
ax.plot(x,beta.pdf(x,3,20),label=r"$\alpha=3, \beta=20$")
ax.plot(x, expovariate.pdf(x,3,20),label=r"$\alpha=3, \beta=20$")
ax.grid(True)
ax.minorticks_on()
ax.legend(loc=9)
setp(ax.get_legend().get_texts(),fontsize='small')
ax.set_ylim(0,2.6)
ax.set_xlabel("x")
ax.set_ylabel("PDF")
#fig.show()
#sleep(999)

fig.savefig("Beta_distribution_pdf.pdf",bbox_inches="tight",pad_inches=.15)
'''