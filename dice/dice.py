__author__ = 'Stephen'

import random
from abc import ABCMeta
from abc import abstractmethod


class Dice(metaclass=ABCMeta):
    @abstractmethod
    def roll(self):
        pass

    def test_rolls(self, num, repetitions):
        max = 0
        min = num * 9
        mean = 0
        rollLog = ""
        for x in range(repetitions):
            roll = self.roll(num)
            mean += roll
            if roll > max:
                max = roll
            if roll < min:
                min = roll
            rollLog += ", " + "{: 6.2f}".format(roll)
            if x % 10 == 0:
                rollLog += '\n'
        mean /= float(repetitions)
        print(rollLog)
        print("Min : " + str(min))
        print("Max : " + str(max))
        print("Mean : " + str(mean))


class DiceMax10(Dice):
    def roll(self, num):
        return random.betavariate(3, 24) * 10.0 * num;


class DiceMax10Wide(Dice):
    def roll(self, num):
        return random.betavariate(2, 15.4) * 10.0 * num;


class DiceMax5(Dice):
    def roll(self, num):
        return random.betavariate(3, 11) * 5.0 * num;


class DiceMax5Wide(Dice):
    def roll(self, num):
        return random.betavariate(2, 7) * 5.0 * num;

        # DiceMax5().testRolls(5, 1000);